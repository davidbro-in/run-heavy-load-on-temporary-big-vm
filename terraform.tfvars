aws_region      = "eu-south-1"
gitlab_url      = "https://gitlab.com"
aud_value       = "https://gitlab.com"
match_field     = "sub"
match_value     = ["project_path:mygroup/*:ref_type:branch:ref:main"]